/**
 * @file
 * @brief Header file defining structures for passing messages between the wra client
 *        and agent
 *
 * @copyright Copyright (C) 2015 Wind River Systems, Inc. All Rights Reserved.
 *
 * @license The right to copy, distribute or otherwise make use of this software may
 * be licensed only pursuant to the terms of an applicable Wind River license
 * agreement.  No license to Wind River intellectual property rights is granted
 * herein.  All rights not licensed by Wind River are reserved by Wind River.
 */
#ifndef WRA_MESSAGES_H
#define WRA_MESSAGES_H

#include "wra_internal.h"
#include "wra_types.h"

/* enumerations */
/** @brief Type of command request */
enum wra_command_request_type
{
	WRA_COMMAND_REQUEST_REGISTER = 0,                /**< @brief Register a new command */
	WRA_COMMAND_REQUEST_DEREGISTER = 1,              /**< @brief Deregister a previous command */
	WRA_COMMAND_REQUEST_EXECUTE = 2                  /**< @brief Execute a command */
};

/** @brief Type of connection request */
enum wra_connect_request_type
{
	WRA_CONNECT_REQUEST_REGISTER = 0,               /**< @brief Register a new client */
	WRA_CONNECT_REQUEST_DEREGISTER = 1              /**< @brief Deregister an existing client */
};

/** @brief Type of message (this should be the first entry on all messages) */
enum wra_message_type
{
	WRA_MESSAGE_TYPE_COMMAND = 1,                   /**< @brief Command message */
	WRA_MESSAGE_TYPE_COMMAND_RESPONSE,              /**< @brief Command response message */
	WRA_MESSAGE_TYPE_CONNECT,                       /**< @brief Connection message */
	WRA_MESSAGE_TYPE_HEARTBEAT,                     /**< @brief Heartbeat message */
	WRA_MESSAGE_TYPE_LOCATION,                      /**< @brief Location message */
	WRA_MESSAGE_TYPE_LOCATION_CONFIG,               /**< @brief Location configuration message */
	WRA_MESSAGE_TYPE_METRIC,                        /**< @brief Metric registration/deregistration message */
	WRA_MESSAGE_TYPE_NOTIFICATION,                  /**< @brief Response message to send to client */
	WRA_MESSAGE_TYPE_SAMPLE,                        /**< @brief Sample publishing message */

	/* this should be the last item */
	WRA_MESSAGE_TYPE_UNKNOWN                        /**< @brief Unknown message */
};

/** @brief Type of metric request */
enum wra_metric_request_type
{
	WRA_METRIC_REQUEST_REGISTER = 0,                /**< @brief Register a new metric */
	WRA_METRIC_REQUEST_DEREGISTER                   /**< @brief Deregister an existing metric */
};

/* structures */
/** @brief Allocator to change the default dynamic allocation mechanism */
struct wra_allocator
{
	void* (*alloc)(size_t);                         /**< @brief Memory allocation function */
	void (*free)(void*);                            /**< @brief Memory free function */
};

/** @brief Structure defining a command message */
struct wra_message_command
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	enum wra_command_request_type request_type;     /**< @brief Type of command request */
	char* request_id;                               /**< @brief Request id associated with this request */
	char* service_name;                             /**< @brief Name of the service */
	char* service_version;                          /**< @brief Version of the service (optional) */
	char* command_name;                             /**< @brief Name of the command */
	size_t parameter_count;                         /**< @brief Number of parameters */
	struct wra_parameter** parameter;               /**< @brief Command parameters (optional) */
};

/** @brief Structure defining a command response message */
struct wra_message_command_response
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	char* request_id;                               /**< @brief Request id associated with this request */
	char* service_name;                             /**< @brief Name of service the executed command belongs to */
	char* service_version;                          /**< @brief Version of service the executed command belongs to */
	char* command_name;                             /**< @brief Name of command that was executed */
	wra_command_status_t command_result;            /**< @brief Result from the command execution */
	wra_data_t response;                            /**< @brief Response data */
};

/** @brief Structure defining a connection message */
struct wra_message_connect
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	enum wra_connect_request_type request_type;     /**< @brief Type of command request */
	char* client_id;                                /**< @brief Name of the service */
};

/** @brief Structure defining the location message */
struct wra_message_location
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	struct wra_location location;                   /**< @brief Location information */
};

/** @brief Structure defining the location configuration message */
struct wra_message_location_config
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	uint8_t samples_max;                            /**< @brief Maximum amount of samples to publish */
	uint8_t samples_min;                            /**< @brief Minimum amount of samples to publish */
};

/** @brief Structure defining the metric message */
struct wra_message_metric
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	enum wra_metric_request_type request_type;      /**< @brief Type of metric request */
	char* source_name;                              /**< @brief Name of the source */
	char* source_version;                           /**< @brief Version fo the source */
	char* metric_name;                              /**< @brief Name of the metric */
	char* metric_version;                           /**< @brief Version of the metric */
	char* metric_units;                             /**< @brief Unit description for the metric */
	wra_type_t metric_type;                         /**< @brief Data type of the samples */
	uint8_t samples_max;                            /**< @brief Maximum amount of samples to publish */
	uint8_t samples_min;                            /**< @brief Minimum amount of samples to publish */
};

/** @brief Structure defining a notification message to a client */
struct wra_message_notification
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	enum wra_notification_type notification_type;   /**< @brief Type of notification */
	wra_data_t response_data;                       /**< @brief Optional response data */
	char* request_id;                               /**< @brief Id of the request the response is for */
	wra_state_t state;                              /**< @brief Current state of the agent */
	wra_status_t status;                            /**< @brief Resulting status */
};

/** @brief Structure defining a heartbeat response message */
struct wra_message_heartbeat_response
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	enum wra_heartbeat_response_type response_type; /**< @brief Type of response */
	char* request_id;                               /**< @brief Request id associated with this request */
};

/** @brief Structure defining the sample message */
struct wra_message_sample
{
	enum wra_message_type message_type;             /**< @brief Type of message */
	char* source_name;                              /**< @brief Name of the source */
	char* source_version;                           /**< @brief Version of the source */
	char* metric_name;                              /**< @brief Name of the metric */
	char* metric_version;                           /**< @brief Version of the metric */
	wra_timestamp_t time_stamp;                     /**< @brief Time sample was published */
	wra_data_t sample_data;                         /**< @brief Sample data payload */
};

/* typedefs */
/** @brief Type defining the dynamic memory allocator object to use */
typedef struct wra_allocator wra_allocator_t;
/** @brief Type defining the enumeration for the request type */
typedef enum wra_command_request_type wra_command_request_type_t;
/** @brief Type defining the command message */
typedef struct wra_message_command wra_message_command_t;
/** @brief Type defining the command response message */
typedef struct wra_message_command_response wra_message_command_response_t;
/** @brief Type defining the connection message */
typedef struct wra_message_connect wra_message_connect_t;
/** @brief Type defining the location message */
typedef struct wra_message_location wra_message_location_t;
/** @brief Type defining the location configuration message */
typedef struct wra_message_location_config wra_message_location_config_t;
/** @brief Type defining the metric message */
typedef struct wra_message_metric wra_message_metric_t;
/** @brief Type defining the notification message */
typedef struct wra_message_notification wra_message_notification_t;
/** @brief Type defining the response message */
typedef struct wra_message_heartbeat_response wra_message_heartbeat_response_t;
/** @brief Type defining the sample message */
typedef struct wra_message_sample wra_message_sample_t;

/* ------------------------------------------------------------------------- */
/* COMMAND MESSAGE */
/**
 * @brief Initializes the command message with default settings
 * @param[in]  msg                     message to initialize
 */
void wra_message_command_initialize( wra_message_command_t* msg );
/**
 * @brief Initializes the command response message with default settings
 * @param[in,out]  msg                           message to initialize
 */
void wra_message_command_response_initialize( wra_message_command_response_t* msg );
/**
 * @brief Reads a command response message from a buffer
 * @param[in]  allocator               optional structure to use to allocate dynamic memory
 * @param[in]  buf                     buffer containing the packed message
 * @param[in]  length                  message size on the buffer
 * @return a newly allocated messasge unpacked from the buffer
 * @see message_command_response_read_release
 */
wra_message_command_response_t* wra_message_command_response_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );
/**
 * @brief Release a dynamically allocated buffer to parsing a packed command response message
 * @param[in]  allocator               optional structure to use to release dynamically allocated memory
 * @param[in]  msg                     buffer to release
 * @see message_command_response_read
 */
void wra_message_command_response_read_release( wra_allocator_t* allocator, wra_message_command_response_t* msg );
/**
 * @brief Serializes a command response message on a buffer
 * @param[in]  msg                     message to serialize
 * @param[out] out                     buffer to serialize data to
 * @param[in]  length                  size of the output buffer
 * @return pointer to the end of the buffer
 * @see message_command_response_write_size
 */
uint8_t* wra_message_command_response_write( const wra_message_command_response_t* msg, uint8_t* out, size_t length );
/**
 * @brief Determines the size of the buffer required to serialize a command response message
 * @param[in]      msg                           message to calculate the required size for
 * @return The size of the buffer required to serialize the message specified
 * @see wra_message_command_response_write
 */
size_t wra_message_command_response_write_size( const wra_message_command_response_t* msg );
/**
 * @brief Reads a command message from a buffer
 * @param[in]  allocator               optional structure to use to allocate dynamic memory
 * @param[in]  buf                     buffer containing the packed message
 * @param[in]  length                  message size on the buffer
 * @return a newly allocated messasge unpacked from the buffer
 * @see message_command_read_release
 */
wra_message_command_t* wra_message_command_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed command message
 * @param[in]  allocator               optional structure to use to release dynamically allocated memory
 * @param[in]  msg                     buffer to release
 * @see message_command_read
 */
void wra_message_command_read_release( wra_allocator_t* allocator, wra_message_command_t* msg );

/**
 * @brief Serializes a command message on a buffer
 * @param[in]  msg                     message to serialize
 * @param[out] out                     buffer to serialize data to
 * @param[in]  length                  size of the output buffer
 * @return pointer to the end of the buffer
 * @see message_command_write_size
 */
uint8_t* wra_message_command_write( const wra_message_command_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a command message
 * @param[in]      msg                           message to calculate the required size for
 * @return The size of the buffer required to serialize the message specified
 * @see message_command_write
 */
size_t wra_message_command_write_size( const wra_message_command_t* msg );

/* ------------------------------------------------------------------------- */
/* CONNECTION MESSAGE */
/**
 * @brief Initializes the connection message with default settings
 * @param[in]  msg                     message to initialize
 */
void wra_message_connect_initialize( wra_message_connect_t* msg );

/**
 * @brief Reads a connection message from a buffer
 * @param[in]  allocator               optional structure to use to allocate dynamic memory
 * @param[in]  buf                     buffer containing the packed message
 * @param[in]  length                  message size on the buffer
 * @return a newly allocated messasge unpacked from the buffer
 * @see message_connect_read_release
 */
wra_message_connect_t* wra_message_connect_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed connection message
 * @param[in]  allocator               optional structure to use to release dynamically allocated memory
 * @param[in]  msg                     buffer to release
 * @see message_connect_read
 */
void wra_message_connect_read_release( wra_allocator_t* allocator, wra_message_connect_t* msg );

/**
 * @brief Serializes a connection message on a buffer
 * @param[in]  msg                     message to serialize
 * @param[out] out                     buffer to serialize data to
 * @param[in]  length                  size of the output buffer
 * @return pointer to the end of the buffer
 * @see message_connect_write_size
 */
uint8_t* wra_message_connect_write( const wra_message_connect_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a conection message
 * @param[in]      msg                           message to calculate the required size for
 * @return The size of the buffer required to serialize the message specified
 * @see message_connect_write
 */
size_t wra_message_connect_write_size( const wra_message_connect_t* msg );

/* ------------------------------------------------------------------------- */
/* LOCATION MESSAGE */
/**
 * @brief Initializes the location message with default settings
 *
 * @param[in]      msg                           message to initialize
 */
void wra_message_location_initialize( wra_message_location_t* msg );

/**
 * @brief Reads a location message from a buffer
 *
 * @param[in]      allocator                     optional structure to use to allocate dynamic memory
 * @param[in]      buf                           buffer containing the packed message
 * @param[in]      length                        message size on the buffer
 *
 * @return a newly allocated messasge unpacked from the buffer
 *
 * @see message_location_read_release
 */
wra_message_location_t* wra_message_location_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed location message
 *
 * @param[in]      allocator                     optional structure to use to release dynamically allocated memory
 * @param[in]      msg                           buffer to release
 *
 * @see message_location_read
 */
void wra_message_location_read_release( wra_allocator_t* allocator, wra_message_location_t* msg );

/**
 * @brief Serializes a location message on a buffer
 *
 * @param[in]      msg                           message to serialize
 * @param[out]     out                           buffer to serialize data to
 * @param[in]      length                        size of the output buffer
 *
 * @return pointer to the end of the buffer
 *
 * @see message_location_write_size
 */
uint8_t* wra_message_location_write( const wra_message_location_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a location message
 *
 * @param[in]      msg                           message to calculate the required size for
 *
 * @return The size of the buffer required to serialize the message specified
 *
 * @see message_location_write
 */
size_t wra_message_location_write_size( const wra_message_location_t* msg );

/* ------------------------------------------------------------------------- */
/* LOCATION CONFIGURATION MESSAGE */
/**
 * @brief Initializes the location configuration message with default settings
 *
 * @param[in]      msg                           message to initialize
 */
void wra_message_location_config_initialize( wra_message_location_config_t* msg );

/**
 * @brief Reads a location configuration message from a buffer
 *
 * @param[in]      allocator                     optional structure to use to allocate dynamic memory
 * @param[in]      buf                           buffer containing the packed message
 * @param[in]      length                        message size on the buffer
 *
 * @return a newly allocated messasge unpacked from the buffer
 *
 * @see message_location_config_read_release
 */
wra_message_location_config_t* wra_message_location_config_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed location configuration message
 *
 * @param[in]      allocator                     optional structure to use to release dynamically allocated memory
 * @param[in]      msg                           buffer to release
 *
 * @see message_location_config_read
 */
void wra_message_location_config_read_release( wra_allocator_t* allocator, wra_message_location_config_t* msg );

/**
 * @brief Serializes a location message on a buffer
 * @param[in]      msg                           message to serialize
 * @param[out]     out                           buffer to serialize data to
 * @param[in]      length                        size of the output buffer
 *
 * @return pointer to the end of the buffer
 *
 * @see message_location_write_config_size
 */
uint8_t* wra_message_location_config_write( const wra_message_location_config_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a location configuration message
 *
 * @param[in]      msg                           message to calculate the required size for
 *
 * @return The size of the buffer required to serialize the message specified
 *
 * @see message_location_config_write
 */
size_t wra_message_location_config_write_size( const wra_message_location_config_t* msg );

/* ------------------------------------------------------------------------- */
/* METRIC MESSAGE */
/**
 * @brief Initializes the metric message with default settings
 * @param[in]  msg                     message to initialize
 */
void wra_message_metric_initialize( wra_message_metric_t* msg );

/**
 * @brief Reads a metric message from a buffer
 * @param[in]  allocator               optional structure to use to allocate dynamic memory
 * @param[in]  buf                     buffer containing the packed message
 * @param[in]  length                  message size on the buffer
 * @return a newly allocated messasge unpacked from the buffer
 * @see message_metric_read_release
 */
wra_message_metric_t* wra_message_metric_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed metric message
 * @param[in]  allocator               optional structure to use to release dynamically allocated memory
 * @param[in]  msg                     buffer to release
 * @see message_metric_read
 */
void wra_message_metric_read_release( wra_allocator_t* allocator, wra_message_metric_t* msg );

/**
 * @brief Serializes a metric message on a buffer
 * @param[in]  msg                     message to serialize
 * @param[out] out                     buffer to serialize data to
 * @param[in]  length                  size of the output buffer
 * @return pointer to the end of the buffer
 * @see message_metric_write_size
 */
uint8_t* wra_message_metric_write( const wra_message_metric_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a location message
 * @param[in]      msg                           message to calculate the required size for
 * @return The size of the buffer required to serialize the message specified
 * @see message_metric_write
 */
size_t wra_message_metric_write_size( const wra_message_metric_t* msg );

/* ------------------------------------------------------------------------- */
/* NOTIFICATION MESSAGE */
/**
 * @brief Initializes the notification message with default settings
 * @param[in,out]  msg                           message to initialize
 */
void wra_message_notification_initialize( wra_message_notification_t* msg );

/**
 * @brief Reads a notification message from a buffer
 * @param[in,out]  allocator                     optional structure to use to allocate dynamic memory
 * @param[in]      buf                           buffer containing the packed message
 * @param[in]      length                        message size on the buffer
 * @return a newly allocated messasge unpacked from the buffer
 * @see message_response_read_release
 */
wra_message_notification_t* wra_message_notification_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed notification message
 * @param[in,out]  allocator                     optional structure to use to release dynamically allocated memory
 * @param[in,out]  msg                           buffer to release
 * @see message_response_read
 */
void wra_message_notification_read_release( wra_allocator_t* allocator, wra_message_notification_t* msg );

/**
 * @brief Serializes a notification message on a buffer
 * @param[in]      msg                           message to serialize
 * @param[out]     out                           buffer to serialize data to
 * @param[in]      length                        size of the output buffer
 * @return pointer to the end of the buffer
 * @see message_response_write_size
 */
uint8_t* wra_message_notification_write( const wra_message_notification_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a notification message
 * @param[in]      msg                           message to calculate the required size for
 * @return The size of the buffer required to serialize the message specified
 * @see message_response_write
 */
size_t wra_message_notification_write_size( const wra_message_notification_t* msg );

/* ------------------------------------------------------------------------- */
/* RESPONSE MESSAGE */
/**
 * @brief Initializes the response message with default settings
 * @param[in,out]  msg                           message to initialize
 */
void wra_message_heartbeat_response_initialize( wra_message_heartbeat_response_t* msg );

/**
 * @brief Reads a response message from a buffer
 * @param[in,out]  allocator                     optional structure to use to allocate dynamic memory
 * @param[in]      buf                           buffer containing the packed message
 * @param[in]      length                        message size on the buffer
 * @return a newly allocated messasge unpacked from the buffer
 * @see message_heartbeat_response_read_release
 */
wra_message_heartbeat_response_t* wra_message_heartbeat_response_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed response message
 * @param[in,out]  allocator                     optional structure to use to release dynamically allocated memory
 * @param[in,out]  msg                           buffer to release
 * @see message_heartbeat_response_read
 */
void wra_message_heartbeat_response_read_release( wra_allocator_t* allocator, wra_message_heartbeat_response_t* msg );

/**
 * @brief Serializes a response message on a buffer
 * @param[in]      msg                           message to serialize
 * @param[out]     out                           buffer to serialize data to
 * @param[in]      length                        size of the output buffer
 * @return pointer to the end of the buffer
 * @see message_heartbeat_response_write_size
 */
uint8_t* wra_message_heartbeat_response_write( const wra_message_heartbeat_response_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a response message
 * @param[in]      msg                           message to calculate the required size for
 * @return The size of the buffer required to serialize the message specified
 * @see message_heartbeat_response_write
 */
size_t wra_message_heartbeat_response_write_size( const wra_message_heartbeat_response_t* msg );

/* ------------------------------------------------------------------------- */
/* SAMPLE MESSAGE */
/**
 * @brief Initializes the sample message with default settings
 * @param[in]  msg                     message to initialize
 */
void wra_message_sample_initialize( wra_message_sample_t* msg );

/**
 * @brief Reads a sample message from a buffer
 * @param[in]  allocator               optional structure to use to allocate dynamic memory
 * @param[in]  buf                     buffer containing the packed message
 * @param[in]  length                  message size on the buffer
 * @return a newly allocated messasge unpacked from the buffer
 * @see message_sample_read_release
 */
wra_message_sample_t* wra_message_sample_read( wra_allocator_t* allocator, const uint8_t* buf, size_t length );

/**
 * @brief Release a dynamically allocated buffer to parsing a packed sample message
 * @param[in]  allocator               optional structure to use to release dynamically allocated memory
 * @param[in]  msg                     buffer to release
 * @see message_sample_read
 */
void wra_message_sample_read_release( wra_allocator_t* allocator, wra_message_sample_t* msg );

/**
 * @brief Serializes a sample message on a buffer
 * @param[in]  msg                     message to serialize
 * @param[out] out                     buffer to serialize data to
 * @param[in]  length                  size of the output buffer
 * @return pointer to the end of the buffer
 * @see message_sample_write_size
 */
uint8_t* wra_message_sample_write( const wra_message_sample_t* msg, uint8_t* out, size_t length );

/**
 * @brief Determines the size of the buffer required to serialize a sample message
 * @param[in]      msg                           message to calculate the required size for
 * @return The size of the buffer required to serialize the message specified
 * @see message_sample_write
 */
size_t wra_message_sample_write_size( const wra_message_sample_t* msg );

/* ------------------------------------------------------------------------- */
/* UTILITY FUNCTIONS */
/**
 * @brief Retrieves a copy of the message header from a mesage stored on the buffer
 * @param[in]  buf                     pointer to the buffer to read from
 * @param[in]  length                  length of the buffer
 *
 * @return the type of message stored on the buffer
 */
enum wra_message_type wra_message_type_get( const uint8_t* buf, size_t length );

#endif /* ifndef WRA_MESSAGES_H */

