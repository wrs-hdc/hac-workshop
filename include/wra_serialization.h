/**
 * @file
 * @brief Header file for supporting serialization of data between the client and
 *        agent.
 *
 * @copyright Copyright (C) 2015 Wind River Systems, Inc. All Rights Reserved.
 *
 * @license The right to copy, distribute or otherwise make use of this software may
 * be licensed only pursuant to the terms of an applicable Wind River license
 * agreement.  No license to Wind River intellectual property rights is granted
 * herein.  All rights not licensed by Wind River are reserved by Wind River.
 */

#ifndef WRA_SERIALIZATION_H
#define WRA_SERIALIZATION_H

#include "wra_types.h" /* for wra_timestamp_t */
#include <stdint.h> /* for uint8_t, uint32_t */
#include <stdlib.h> /* for size_t */

/**
 * @brief Structure to define the storage of binary data
 */
struct wra_raw_data
{
	uint8_t* data; /**< @brief Pointer to the binary data */
	size_t length; /**< @brief Amount of binary data */
};

/** @brief Defines the type for raw data */
typedef struct wra_raw_data wra_raw_data_t;

/**
 * @brief Reads a 32-bit floating point from the buffer
 *
 * @param[in]      in                            buffer to read from
 * @param[out]     data                          destination buffer to read to
 *
 * @return A pointer to the buffer after the read
 *
 * @see wra_float32_write
 */
const uint8_t* wra_float32_read( const uint8_t* in, float* data );

/**
 * @brief Serializes a 32-bit floating point into a buffer
 *
 * @param[in]      data                          data to write to the buffer
 * @param[out]     out                           buffer to write to
 * @param[in]      length                        length of the output buffer
 *
 * @return A pointer to the end to buffer after serializing the data
 *
 * @see wra_float32_read
 * @see wra_float32_write_size
 */
uint8_t* wra_float32_write( float data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold a 32-bit floating-point
 *
 * @param[in]      data                          data to calculate the space requirements for
 *
 * @return The amount of space required to hold the data when serialized
 *
 * @see wra_float32_write
 */
size_t wra_float32_write_size( float data );

/**
 * @brief Reads a 64-bit floating point from the buffer
 *
 * @param[in]      in                            buffer to read from
 * @param[out]     data                          destination buffer to read to
 *
 * @return A pointer to the buffer after the read
 *
 * @see wra_float64_write
 */
const uint8_t* wra_float64_read( const uint8_t* in, double* data );

/**
 * @brief Serializes a 64-bit floating point into a buffer
 *
 * @param[in]      data                          data to write to the buffer
 * @param[out]     out                           buffer to write to
 * @param[in]      length                        length of the output buffer
 *
 * @return A pointer to the end to buffer after serializing the data
 *
 * @see wra_float64_read
 * @see wra_float64_write_size
 */
uint8_t* wra_float64_write( double data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold a 64-bit floating-point
 *
 * @param[in]      data                          data to calculate the space requirements for
 *
 * @return The amount of space required to hold the data when serialized
 *
 * @see wra_float64_write
 */
size_t wra_float64_write_size( double data );

/**
 * @brief Reads a raw data from the buffer
 * @param[in]     in                   buffer to read from
 * @param[in,out] dest                 destination buffer to read to, set with maximum length
 * @return A pointer to the buffer after the read
 * @see wra_raw_read_size
 * @see wra_raw_write
 */
const uint8_t* wra_raw_read( const uint8_t* in, wra_raw_data_t* dest );

/**
 * @brief Returns the size of raw data in the buffer
 * @param[in]  in                      pointer to a raw buffer
 * @return the length of the raw data
 * @see wra_read_read
 */
size_t wra_raw_read_size( const uint8_t* in );

/**
 * @brief Serializes binary data into a buffer
 * @param[in]  data                    data to write to the buffer
 * @param[out] out                     buffer to write to
 * @param[in]  length                  length of the output buffer
 * @return A pointer to the end to buffer after serializing the data
 * @see wra_write
 * @see wra_raw_write_size
 */
uint8_t* wra_raw_write( const wra_raw_data_t* data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold the data specfied
 * @param[in]  data                    data to calculate the space requirements for
 * @return The amount of space required to hold the data when serialized
 * @see wra_raw_write
 */
size_t wra_raw_write_size( const wra_raw_data_t* data );

/**
 * @brief Reads a string from the buffer
 * @param[in]  in                      buffer to read from
 * @param[out] dest                    destination buffer to read to
 * @param[in]  length                  size of the destination buffer
 * @return A pointer to the buffer after the read
 * @see wra_string_read_size
 * @see wra_string_write
 */
const uint8_t* wra_string_read( const uint8_t* in, char* dest, size_t length );

/**
 * @brief Returns the size of string in the buffer
 * @param[in]  in                      pointer to a raw buffer
 * @return the length of the string
 * @see wra_string_read
 */
size_t wra_string_read_size( const uint8_t* in );

/**
 * @brief Serializes a string into a buffer
 * @param[in]  data                    data to write to the buffer
 * @param[out] out                     buffer to write to
 * @param[in]  length                  length of the output buffer
 * @return A pointer to the end to buffer after serializing the data
 * @see wra_string_read
 * @see wra_string_write_size
 */
uint8_t* wra_string_write( const char* data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold the data specfied
 * @param[in]  data                    data to calculate the space requirements for
 * @return The amount of space required to hold the data when serialized
 * @see wra_string_write
 */
size_t wra_string_write_size( const char* data );

/**
 * @brief Reads time from the buffer
 * @param[in]  in                      buffer to read from
 * @param[out] dest                    destination buffer to read to
 * @return A pointer to the buffer after the read
 * @see wra_time_read_size
 * @see wra_time_write
 */
const uint8_t* wra_time_read( const uint8_t* in, wra_timestamp_t* dest );

/**
 * @brief Returns the size of time in the buffer
 * @param[in]  in                      pointer to a raw buffer
 * @return the length of the time
 * @see wra_time_read
 */
size_t wra_time_read_size( const uint8_t* in );

/**
 * @brief Serializes a time into a buffer
 * @param[in]  data                    data to write to the buffer
 * @param[out] out                     buffer to write to
 * @param[in]  length                  length of the output buffer
 * @return A pointer to the end to buffer after serializing the data
 * @see wra_time_read
 * @see wra_time_write_size
 */
uint8_t* wra_time_write( const wra_timestamp_t* data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold the data specfied
 * @param[in]  data                    data to calculate the space requirements for
 * @return The amount of space required to hold the data when serialized
 * @see wra_time_write
 */
size_t wra_time_write_size( const wra_timestamp_t* data );

/**
 * @brief Reads a 8-bit unsigned integer from the buffer
 * @param[in]  in                      buffer to read from
 * @param[out] data                    destination buffer to read to
 * @return A pointer to the buffer after the read
 * @see wra_uint8_write
 */
const uint8_t* wra_uint8_read( const uint8_t* in, uint8_t* data );

/**
 * @brief Serializes a 8-bit unsigned integer into a buffer
 * @param[in]  data                    data to write to the buffer
 * @param[out] out                     buffer to write to
 * @param[in]  length                  length of the output buffer
 * @return A pointer to the end to buffer after serializing the data
 * @see wra_uint8_read
 * @see wra_uint8_write_size
 */
uint8_t* wra_uint8_write( uint8_t data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold a 8-bit unsigned integer
 * @param[in]  data                    data to calculate the space requirements for
 * @return The amount of space required to hold the data when serialized
 * @see wra_uint8_write
 */
size_t wra_uint8_write_size( uint8_t data );

/**
 * @brief Reads a 16-bit unsigned integer from the buffer
 * @param[in]  in                      buffer to read from
 * @param[out] data                    destination buffer to read to
 * @return A pointer to the buffer after the read
 * @see wra_uint16_write
 */
const uint8_t* wra_uint16_read( const uint8_t* in, uint16_t* data );

/**
 * @brief Serializes a 16-bit unsigned integer into a buffer
 * @param[in]  data                    data to write to the buffer
 * @param[out] out                     buffer to write to
 * @param[in]  length                  length of the output buffer
 * @return A pointer to the end to buffer after serializing the data
 * @see wra_uint16_read
 * @see wra_uint16_write_size
 */
uint8_t* wra_uint16_write( uint16_t data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold a 16-bit unsigned integer
 * @param[in]  data                    data to calculate the space requirements for
 * @return The amount of space required to hold the data when serialized
 * @see wra_uint16_write
 */
size_t wra_uint16_write_size( uint16_t data );

/**
 * @brief Reads a 32-bit unsigned integer from the buffer
 * @param[in]  in                      buffer to read from
 * @param[out] data                    destination buffer to read to
 * @return A pointer to the buffer after the read
 * @see wra_uint32_write
 */
const uint8_t* wra_uint32_read( const uint8_t* in, uint32_t* data );

/**
 * @brief Serializes a 32-bit unsigned integer into a buffer
 * @param[in]  data                    data to write to the buffer
 * @param[out] out                     buffer to write to
 * @param[in]  length                  length of the output buffer
 * @return A pointer to the end to buffer after serializing the data
 * @see wra_uint32_read
 * @see wra_uint32_write_size
 */
uint8_t* wra_uint32_write( uint32_t data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold a 32-bit unsigned integer
 * @param[in]  data                    data to calculate the space requirements for
 * @return The amount of space required to hold the data when serialized
 * @see wra_uint32_write
 */
size_t wra_uint32_write_size( uint32_t data );

/**
 * @brief Reads a 64-bit unsigned integer from the buffer
 * @param[in]  in                      buffer to read from
 * @param[out] data                    destination buffer to read to
 * @return A pointer to the buffer after the read
 * @see wra_uint64_write
 */
const uint8_t* wra_uint64_read( const uint8_t* in, uint64_t* data );

/**
 * @brief Serializes a 64-bit unsigned integer into a buffer
 * @param[in]  data                    data to write to the buffer
 * @param[out] out                     buffer to write to
 * @param[in]  length                  length of the output buffer
 * @return A pointer to the end to buffer after serializing the data
 * @see wra_uint64_read
 * @see wra_uint64_write_size
 */
uint8_t* wra_uint64_write( uint64_t data, uint8_t* out, size_t length );

/**
 * @brief Calculates the amount of space required to hold a 64-bit unsigned integer
 * @param[in]  data                    data to calculate the space requirements for
 * @return The amount of space required to hold the data when serialized
 * @see wra_uint64_write
 */
size_t wra_uint64_write_size( uint64_t data );

#endif /* ifndef WRA_SERIALIZATION_H */

