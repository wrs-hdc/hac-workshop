/**
 * @file
 * @brief Header file for connection functionality the Wind River Internet of Things (IoT) library
 *
 * @copyright Copyright (C) 2015-2016 Wind River Systems, Inc. All Rights Reserved.
 *
 * @license The right to copy, distribute or otherwise make use of this software may
 * be licensed only pursuant to the terms of an applicable Wind River license
 * agreement.  No license to Wind River intellectual property rights is granted
 * herein.  All rights not licensed by Wind River are reserved by Wind River.
 */
#ifndef WRA_CONNECTION_H
#define WRA_CONNECTION_H

/* includes */
#include "wra_types.h"
#include <unistd.h> /* for ssize_t */

#ifdef __cplusplus
extern "C" {
#endif /* ifdef __cplusplus */

/* typedefs */
/** @brief Type for a connection between the agent and a client */
typedef struct wra_connection wra_connection_t;

/* functions */
/**
 * @brief Closes a connection to the agent
 *
 * @param[in,out]  connection                    connection to close
 *
 * @retval WRA_STATUS_BAD_PARAMETER              bad parameter passed
 * @retval WRA_STATUS_SUCCESS                    success
 */
WRA_API wra_status_t wra_connection_close( wra_connection_t* connection );

/**
 * @brief Opens a new connection to an agent
 *
 * @param[in,out]  lib_handle                    library handle
 * @param[in,out]  connection                    location to store connection details
 * @param[in]      name                          name of the connection
 * @param[in]      flags                         type of connection (O_RDONLY, O_WRONLY, O_RDWR)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              bad parameter passed
 * @retval WRA_STATUS_FAILURE                    failed to open the queue
 * @retval WRA_STATUS_NO_MEMORY                  failed to allocate space for the buffer
 * @retval WRA_STATUS_SUCCESS                    success
 */
WRA_API wra_status_t wra_connection_open( wra_t* lib_handle, wra_connection_t* connection,
	const char* name, int flags );

/**
 * @brief Receives data allocated on the buffer to the agent
 *
 * @param[in,out]  connection                    conection to read from
 * @param[out]     bytes_received                amount of data received (optional)
 * @param[in]      abs_time_out                  absolute maximum time to wait for message reception (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to the function
 * @retval WRA_STATUS_BAD_REQUEST                invalid request: buffer not large enough
 * @retval WRA_STATUS_FAILURE                    failed to receive the message
 * @retval WRA_STATUS_NOT_INITIALIZED            the connection is not initialized
 * @retval WRA_STATUS_SUCCESS                    success
 * @retval WRA_STATUS_TIMED_OUT                  timed out before being able to send
 * @retval WRA_STATUS_TRY_AGAIN                  queue is empty and non-block flag is set
 */
WRA_API wra_status_t wra_connection_receive( wra_connection_t* connection, ssize_t* bytes_received,
	const wra_timestamp_t* abs_time_out );

/**
 * @brief Removes a connection to the agent
 *
 * @param[in,out]  connection                    connection to remore
 *
 * @retval WRA_STATUS_BAD_PARAMETER              bad parameter passed
 * @retval WRA_STATUS_SUCCESS                    success
 */
WRA_API wra_status_t wra_connection_remove( wra_connection_t* connection );

/**
 * @brief Sends data allocated on the buffer to the to the agent
 *
 * @param[in,out]  connection                    connection to use to send the data
 * @param[in]      len                           amount of data to send
 * @param[in]      abs_time_out                  absolute maximum time to wait for data to be sent (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to the function
 * @retval WRA_STATUS_FAILURE                    failed to write the message
 * @retval WRA_STATUS_FULL                       the queue is full
 * @retval WRA_STATUS_NOT_INITIALIZED            the connection is not initialized
 * @retval WRA_STATUS_SUCCESS                    success
 * @retval WRA_STATUS_TIMED_OUT                  timed out before being able to send
 */
WRA_API wra_status_t wra_connection_send( wra_connection_t* connection, size_t len,
	const wra_timestamp_t* abs_time_out );

#ifdef __cplusplus
}
#endif /* ifdef __cplusplus */

#endif /* ifndef WRA_CONNECTION_H */

