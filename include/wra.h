/**
 * @file
 * @brief Header file using the Wind River Agent library
 *
 * @copyright Copyright (C) 2014-2016 Wind River Systems, Inc. All Rights Reserved.
 *
 * @license The right to copy, distribute or otherwise make use of this software may
 * be licensed only pursuant to the terms of an applicable Wind River license
 * agreement.  No license to Wind River intellectual property rights is granted
 * herein.  All rights not licensed by Wind River are reserved by Wind River.
 */
#ifndef WRA_H
#define WRA_H

/* includes */
#include "wra_command.h"
#include "wra_common.h"
#include "wra_compat.h"
#include "wra_location.h"
#include "wra_metric.h"
#include "wra_service.h"
#include "wra_source.h"
#include "wra_types.h"

#endif /* ifndef WRA_H */

