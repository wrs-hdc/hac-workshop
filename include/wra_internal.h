/**
 * @file
 * @brief Header file for internal structures within the Wind River Agent library
 *
 * @copyright Copyright (C) 2015-2016 Wind River Systems, Inc. All Rights Reserved.
 *
 * @license The right to copy, distribute or otherwise make use of this software may
 * be licensed only pursuant to the terms of an applicable Wind River license
 * agreement.  No license to Wind River intellectual property rights is granted
 * herein.  All rights not licensed by Wind River are reserved by Wind River.
 */

#ifndef WRA_INTERNAL_H
#define WRA_INTERNAL_H

#include "wra_connection.h"    /* for wra connection functions */
#include "wra_serialization.h" /* for wra_raw_data */
#include "wra_types.h"         /* for wra_data */

#include <limits.h>     /* for PATH_MAX */
#include <mqueue.h>     /* for posix message queue */
#include <string.h>     /* for memset */

#ifdef __cplusplus
extern "C" {
#endif /* ifdef __cplusplus */

/**
 * @def UNUSED
 * @brief Macro used to define an unused attribute to the compiler
 */
#ifdef __GNUC__
#	define UNUSED(x) PARAM_ ## x __attribute((__unused__))
#else
#	define  __attribute__(x) /* gcc specific */
#	define UNUSED(x)
#endif /* ifdef __GNUC__ */

/* defines */
/** @brief Flag indicating whether 'accuracy' field is set */
#define WRA_LOCATION_FLAG_ACCURACY               (0x01)
/** @brief Flag indicating whether 'altitude' field is set */
#define WRA_LOCATION_FLAG_ALTITUDE               (0x02)
/** @brief Flag indicating whether 'altitude_accuracy' field is set */
#define WRA_LOCATION_FLAG_ALTITUDE_ACCURACY      (0x04)
/** @brief Flag indicating whether 'heading' field is set */
#define WRA_LOCATION_FLAG_HEADING                (0x08)
/** @brief Flag indicating whether 'source_type' field is set */
#define WRA_LOCATION_FLAG_SOURCE                 (0x10)
/** @brief Flag indicating whether 'speed' field is set */
#define WRA_LOCATION_FLAG_SPEED                  (0x20)

/** @brief Number of microseconds in millisecond */
#define WRA_MICROSECONDS_IN_MILLISECOND 1000u
/** @brief Number of milliseconds in a second */
#define WRA_MILLISECONDS_IN_SECOND 1000u
/** @brief Number of milliseconds in a nanosecond */
#define WRA_MILLISECONDS_IN_NANOSECONDS 1000000u
/** @brief Number of nanoseconds in a second */
#define WRA_NANOSECONDS_IN_SECOND 1000000000uL

/** @brief Maximum length of a client id */
#define ID_MAX_LEN 42u
/** @brief Maximum length of a service, command, parameter, telemetry name */
#define NAME_MAX_LEN  64u
/** @brief Maximum length of a version */
#define VERSION_MAX_LEN  16u
/** @brief Maximum number of parameters */
#define PARAM_MAX  7u
/** @brief Default message priority */
#define MSGQ_PRIORITY 10

/** @brief Enumeration defining the index of queues between a client and agent */
enum
{
	QUEUE_INDEX_COMMAND = 0,                        /**< @brief Command queue index */
	QUEUE_INDEX_METRIC,                             /**< @brief Metric queue index */
	QUEUE_INDEX_NOTIFICATION,                       /**< @brief Notification queue index */
	QUEUE_INDEX_REQUEST,                            /**< @brief Request queue index */

	/* This should be the last item */
	QUEUE_INDEX_LAST                                /**< @brief Last queue index (not a real queue) */
};

/** @brief Structure for passing information to agent */
struct wra_connection
{
	struct wra* parent;                             /**< @brief Parent connection */
	uint8_t* buffer;                                /**< @brief Internal buffer used to rx & tx */
	size_t buffer_len;                              /**< @brief Size of the internal buffer */
	wra_bool_t created_q;                           /**< @brief Whether this device owns the queue */
	mqd_t msgq_id;                                  /**< @brief Message queue id */
	char msgq_path[PATH_MAX];                       /**< @brief Message queue name */
};

/** @brief Defines data stored within the library */
struct wra_data
{
	wra_bool_t has_value;                           /**< @brief Whether a value has been assigned */
	wra_type_t type;                                /**< @brief Type of data */

	/** @brief Value of the data */
	union
	{
		wra_bool_t boolean;                            /**< @brief Boolean value */
		int32_t integer;                               /**< @brief Integer value */
		struct wra_raw_data raw;                       /**< @brief Raw value */
		double real;                                   /**< @brief Real value */
		char* string;                                  /**< @brief String value */
	} value;
};

/** @brief Defines a parameter to a command */
struct wra_parameter
{
	char name[NAME_MAX_LEN];                        /**< @brief Name of the parameter */
	struct wra_data data;                           /**< @brief Holds data of the parameter */
};

/** @brief Commands registered with the agent */
struct wra_command
{
	struct wra_service* parent;                     /**< @brief Handle to the parent service */
	char command_name[NAME_MAX_LEN];                /**< @brief Name of the command */
	wra_bool_t is_registered;                       /**< @brief Command has been registered with the cloud */
	wra_command_callback_t* callback;               /**< @brief Callback to run */
	char script[PATH_MAX];                          /**< @brief Script to execute */
	size_t parameter_count;                         /**< @brief Number of parameters */
	struct wra_parameter parameter[PARAM_MAX];      /**< @brief Parameters for the command */
	void* user_data;                                /**< @brief User data to pass to the callback */
	void* compat_data;                              /**< @brief Heap allocated data for backwards compatibility */
	struct wra_command* command_next;               /**< @brief Next command */
	struct wra_command* command_prev;               /**< @brief Previous command */
};

/** @brief Structure defining a command request from the cloud */
struct wra_command_request
{
	char account_id[ID_MAX_LEN];                    /**< @brief Account Identifier of the request */
	char command_name[NAME_MAX_LEN];                /**< @brief Name of the command */
	char request_id[ID_MAX_LEN];                    /**< @brief Request id from the cloud */
	char service_name[NAME_MAX_LEN];                /**< @brief Name of the service */
	char service_version[VERSION_MAX_LEN];          /**< @brief Name of the service version (optional) */
	size_t parameter_count;                         /**< @brief Number of parameters in the request */
	struct wra_parameter parameter[PARAM_MAX];      /**< @brief Array of params for the command */
	struct wra_data response;                       /**< @brief Location of command response if any */
};

/** @brief Location information */
struct wra_location
{
	double accuracy;                                /**< @brief Accuracy of latitude & longitude in metres */
	double altitude;                                /**< @brief Altitude in metres */
	double altitude_accuracy;                       /**< @brief Range of the altitude in metres */
	double heading;                                 /**< @brief Direction heading */
	unsigned int flags;                             /**< @brief Flags associated with structure */
	double latitude;                                /**< @brief Latitude in degrees */
	double longitude;                               /**< @brief Longitude in degrees */
	enum wra_location_source source;                /**< @brief Location source type */
	double speed;                                   /**< @brief Speed being currently travelled in metres/second */
	char tag[NAME_MAX_LEN];                         /**< @brief Location tag */
	wra_timestamp_t time_stamp;                     /**< @brief Time data was produced */
};

/** @brief Holds information about a possible service to run */
struct wra_service
{
	struct wra* lib_handle;                         /**< @brief Handle to the library */
	char service_name[NAME_MAX_LEN];                /**< @brief Name of the service */
	char service_version[VERSION_MAX_LEN];          /**< @brief Version of the service */
	struct wra_command* command_first;              /**< @brief First registered command */
	struct wra_command* command_last;               /**< @brief Last registered command */
	struct wra_service* service_next;               /**< @brief Next service */
	struct wra_service* service_prev;               /**< @brief Previous service */
};

/** @brief Holds information about sources provided by a client */
struct wra_source
{
	struct wra* lib_handle;                         /**< @brief Handle to the library */
	char source_name[NAME_MAX_LEN];                 /**< @brief Name of the source */
	char source_version[VERSION_MAX_LEN];           /**< @brief Version of the source */
	struct wra_metric* metric_first;                /**< @brief First reigstered metric */
	struct wra_metric* metric_last;                 /**< @brief Last registered metric */
	struct wra_source* source_next;                 /**< @brief Next source */
	struct wra_source* source_prev;                 /**< @brief Previous source */
};

/** @brief Holds information about a metric provided by a source of a client */
struct wra_metric
{
	struct wra_source* parent;                      /**< @brief Handle to the parent source */
	char metric_name[NAME_MAX_LEN];                 /**< @brief Name of the metric */
	char metric_version[VERSION_MAX_LEN];           /**< @brief Version of the metric */
	char metric_units[NAME_MAX_LEN];                /**< @brief Units for the metric */
	enum wra_type metric_type;                      /**< @brief Metric data type */
	uint8_t samples_max;                            /**< @brief Maximum number of samples to publish */
	uint8_t samples_min;                            /**< @brief Minimum number of samples to publish */
	struct wra_metric* metric_next;                 /**< @brief Next metric */
	struct wra_metric* metric_prev;                 /**< @brief Previous metric */
};

/** @brief Type of notification */
enum wra_notification_type
{
	WRA_NOTIFICATION_UNKNOWN = 0,                   /**< @brief Default notification type */
	WRA_NOTIFICATION_CLIENT_HEARTBEAT,              /**< @brief Client heartbeat notification */
	WRA_NOTIFICATION_CLIENT_REGISTERED,             /**< @brief Client registered notification */
	WRA_NOTIFICATION_COMMAND_REGISTERED,            /**< @brief Command registered notification */
	WRA_NOTIFICATION_STATE_CHANGED                  /**< @brief State change notification */
};

/** @brief Type of heartbeat response */
enum wra_heartbeat_response_type
{
	WRA_RESPONSE_CLIENT_HEARTBEAT                   /**< @brief Client heartbeat response */
};

/** @brief Holds information about a sample published under a metric */
struct wra_sample
{
	struct wra_metric* parent;                      /**< @brief Handle to the parent metric */
	wra_timestamp_t time_stamp;                     /**< @brief Time stamp sample was published at */
	struct wra_data data;                           /**< @brief Data value for the sample */
};

/** @brief Enumeration for the type of telemetry */
enum wra_telemetry_type
{
	WRA_TELEMETRY_ALARM = 1u,                       /**< @brief Telemetry contains alarm object */
	WRA_TELEMETRY_EVENT,                            /**< @brief Telemetry contains event object */
	WRA_TELEMETRY_METRIC,                           /**< @brief Telemetry contains metric object */
};

/** @brief Structure containing information about a telemetry object */
struct wra_telemetry
{
	enum wra_telemetry_type type;                   /**< @brief Type of telemetry */
	struct wra_data data;                           /**< @brief Temporary data of the telemetry */
	/** @brief Backwards-compatible object for holding information */
	union
	{
		wra_metric_t* metric;                          /**< @brief Metric object this telemetry represents */
	} object;
	wra_bool_t      is_active;                      /**< @brief Telemetry is active (alarms and events) */
	wra_bool_t      time_stamp_has_value;           /**< @brief Whether a value has been set for the time stamp */
	wra_timestamp_t time_stamp;                     /**< @brief Time stamp of the telemetry */
};

/** @brief Internal information for the client and connection data */
struct wra
{
	gid_t agent_gid;                                     /**< @brief Group id the agent runs under */
	uid_t agent_uid;                                     /**< @brief User id the agent runs under */
	wra_state_t agent_state;                             /**< @brief Current state of the agent */
	struct wra_connection client_queue;                  /**< @brief Comon queue for registering/deregistering the client */
	struct wra_connection data_queue[QUEUE_INDEX_LAST];  /**< @brief Various queues unique for each client */
	char id[ID_MAX_LEN];                                 /**< @brief Identifier of the client */
	wra_log_callback_t* logger;                          /**< @brief Function to call on log message */
	void* logger_user_data;                              /**< @brief User data to pass to the log function */
	pthread_cond_t notification_cond;                    /**< @brief Condition variable signalling when a notification arrives */
	pthread_mutex_t notification_mutex;                  /**< @brief Mutex to lock notification condition variable */
	pthread_t notification_thread;                       /**< @brief Notification thread */
	wra_status_t notification_status;                    /**< @brief Result of last notification message */
	enum wra_notification_type notification_type;        /**< @brief Type of notification that was received */
	wra_state_callback_t* state_callback;                /**< @brief Function to call on state change */
	void* state_user_data;                               /**< @brief User data to pass to state change function */

	struct wra_service* service_first;                   /**< @brief First registered service */
	struct wra_service* service_last;                    /**< @brief Last registered service */
	struct wra_source* source_first;                     /**< @brief First registered source */
	struct wra_source* source_last;                      /**< @brief Last registered source */
};

/** @brief Type for a structure containing variable data */
typedef struct wra_data wra_data_t;
/** @brief Type defining the notification types */
typedef enum wra_notification_type wra_notification_type_t;
/** @brief Type defining the response types */
typedef enum wra_heartbeat_response_type wra_heartbeat_response_type_t;
/** @brief Type for a parameter */
typedef struct wra_parameter wra_parameter_t;

/**
 * @brief Internal function to log data
 *
 * @param[in]      lib_handle                    library handle
 * @param[in]      log_level                     log level of the message
 * @param[in]      log_msg_fmt                   message to log in printf format
 * @param[in]      ...                           replacement values as specified by the format
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to function
 * @retval WRA_STATUS_SUCCESS                    on success
 *
 * @see wra_log_callback
 */
wra_status_t wra_log( const wra_t* lib_handle, wra_log_level_t log_level, const char* log_msg_fmt, ... )
	__attribute__((format(printf,3,4)));

/**
 * @brief Waits for a notification packet to arrive
 *
 * @param[in]      lib_handle                    library handle
 * @param[in]      type                          notification type to wait for
 * @param[in]      abs_time_out                  absolute maximum time to wait for the notification (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              bad parameter passed to the function
 * @retval WRA_STATUS_FAILURE                    internal system error return by pthread calls
 * @retval WRA_STATUS_SUCCESS                    success
 * @retval WRA_STATUS_TIMED_OUT                  time out value reached
 */
wra_status_t wra_common_notification_wait( wra_t* lib_handle, wra_notification_type_t type,
	const wra_timestamp_t* abs_time_out );

/**
 * @brief Converts a wait time in milliseconds to an absolute time
 *
 * @param[out]     absolute_time                 pointer to a time stamp structure, this is updated
 *                                               with the current time plus the relative time
 * @param[in]      relative_time                 time relative to the current clock in milliseconds
 *
 * @retval NULL                                  if relative time is 0 or the absolute time is NULL
 * @retval !NULL                                 pointer to the absolute time updated with the
 *                                               relative time
 */
wra_timestamp_t* wra_common_time_relative_to_absolute( wra_timestamp_t* absolute_time,
	wra_millisecond_t relative_time );

/**
 * @brief Implementation for deregistering a command from the cloud
 *
 * @param[in]      command                       command to deregister
 * @param[in]      abs_time_out                  absolute maximum time to wait for
 *                                               deregistration (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to function
 * @retval WRA_STATUS_NO_MEMORY                  not enough system memory
 * @retval WRA_STATUS_NOT_INITIALIZED            library not initialized with the cloud
 * @retval WRA_STATUS_SUCCESS                    on success
 * @retval WRA_STATUS_TIMED_OUT                  timed out while waiting for deregistration
 */
wra_status_t wra_command_deregister_implementation( wra_command_t* command,
	const wra_timestamp_t* abs_time_out );

/**
 * @brief Implementation for registering & deregisteing commands
 *
 * @param[in]      parent                        parent service (NULL to deregister)
 * @param[in]      command                       command to register or unregister
 * @param[in]      abs_time_out                  absolute maximum time to wait (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to the function
 * @retval WRA_STATUS_FAILURE                    failed to write the message
 * @retval WRA_STATUS_FULL                       the queue is full
 * @retval WRA_STATUS_NO_MEMORY                  not enough memory available to generate message
 * @retval WRA_STATUS_NOT_INITIALIZED            the connection is not initialized
 * @retval WRA_STATUS_SUCCESS                    on success
 * @retval WRA_STATUS_TIMED_OUT                  timed out before being able to register or deregister
 */
wra_status_t wra_command_register_implementation( struct wra_service* parent,
	struct wra_command* command, const wra_timestamp_t* abs_time_out );

/**
 * @brief Implementation for destroying a previously allocated command
 *
 * @param[in]      command                       command to destroy
 * @param[in]      abs_time_out                  absolute maximum time to wait (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to function
 * @retval WRA_STATUS_SUCCESS                    on success
 * @retval WRA_STATUS_TIMED_OUT                  timed out while waiting for command deallocation
 */
wra_status_t wra_command_free_implementation( wra_command_t* command, const wra_timestamp_t* abs_time_out );

/**
 * @brief Implementation for destroying a previously allocated metric
 *
 * @param[in]      metric                        metric to destroy
 * @param[in]      abs_time_out                  absolute maximum time to wait (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to function
 * @retval WRA_STATUS_SUCCESS                    on success
 * @retval WRA_STATUS_TIMED_OUT                  timed out while waiting for deallocation
 */
wra_status_t wra_metric_free_implementation( wra_metric_t* metric, const wra_timestamp_t* abs_time_out );

/**
 * @brief Implementation for registering & deregisteing metrics
 *
 * @param[in,out]  parent                        parent source (NULL to deregister)
 * @param[in,out]  metric                        metric to register or unregister
 * @param[in]      abs_time_out                  absolute maximum time to wait (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to the function
 * @retval WRA_STATUS_FAILURE                    failed to write the message
 * @retval WRA_STATUS_FULL                       the queue is full
 * @retval WRA_STATUS_NOT_INITIALIZED            the connection is not initialized
 * @retval WRA_STATUS_SUCCESS                    success
 * @retval WRA_STATUS_TIMED_OUT                  timed out before being able to send
 */
wra_status_t wra_metric_register_implementation( struct wra_source* parent, struct wra_metric* metric,
	const wra_timestamp_t* abs_time_out );

/**
 * @brief Implementation for destroying a previously allocated service
 *
 * @param[in,out]  service                       service to destroy
 * @param[in]      abs_time_out                  absolute maximum time to wait (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to function
 * @retval WRA_STATUS_SUCCESS                    on success
 * @retval WRA_STATUS_TIMED_OUT                  time out value reached
 */
wra_status_t wra_service_free_implementation( wra_service_t* service, const wra_timestamp_t* abs_time_out );

/**
 * @brief Implementation for destroying a previously allocated source
 *
 * @param[in]      source                        source to destroy
 * @param[in]      abs_time_out                  absolute maximum time to wait (optional)
 *
 * @retval WRA_STATUS_BAD_PARAMETER              invalid parameter passed to function
 * @retval WRA_STATUS_SUCCESS                    on success
 * @retval WRA_STATUS_TIMED_OUT                  time out value reached
 */
wra_status_t wra_source_free_implementation( wra_source_t* source, const wra_timestamp_t* abs_time_out );

#ifdef __cplusplus
}
#endif /* ifdef __cplusplus */

#endif /* ifndef WRA_INTERNAL_H */

