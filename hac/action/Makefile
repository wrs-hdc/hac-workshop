# Copyright (c) 2015, Wind River Systems, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

#
# Main Makefile that is invoked when the C9 build buttons/menus are
# used.  This is invoked in the contect of a BASH shell with all of
# the normal Wind River Linux sysroot variables set.
#

-include app.mk

#
# If WIND_PROJECT_ROOT is in the environment, set the EXEDIR to be the
# bin subdirectory.  If not, maintain the legacy behaviour and put it in
# the obj subdirectory.
# 
# Additioanlly, if WIND_PROJECT_ROOT is set, use it for SOURCEDIR else
# maintain the legacy behaviour and make it `pwd`.
#
ifeq ($(WIND_PROJECT_ROOT),)
EXEDIR = $(shell pwd)/obj
SOURCEDIR=$(WIND_PROJECT_ROOT)
else
EXEDIR = $(WIND_PROJECT_ROOT)/bin
SOURCEDIR=$(shell pwd)
endif

#
# If WIND_PROJECT_NAME is in the environment, set the EXE name to be the
# same as WIND_PROJECT_NAME.  If not, maintain the legacy behaviour and set
# it to be the name in resources.mk or the of the top level project 
# directory
#
ifeq ($(WIND_PROJECT_NAME),)
-include resources.mk
ifeq ($(EXE),)
EXE= $(shell basename `pwd`)
endif
else
EXE=$(WIND_PROJECT_NAME)
endif

OBJDIR = obj

#
# Find all source files in the project tree that are not in a .dot directory
# or a .dot file.
#
CFILES = $(shell find $(SOURCEDIR) -name '*.c' | grep  -v  '/\.[a-zA-Z]*')
CXXFILES = $(shell find $(SOURCEDIR) -name '*.cc' -o -name '*.cpp' | grep -v  '/\.[a-zA-Z]*')
ASFILES = $(shell find $(SOURCEDIR) -name '*.s' | grep  -v  '/\.[a-zA-Z]*')

OBJS = $(addsuffix .o,$(notdir $(basename $(CFILES)) $(basename $(ASFILES)) $(basename $(CXXFILES))))
OBJS := $(addprefix $(OBJDIR)/,$(OBJS))

$(foreach f, $(CFILES) $(ASFILES) $(CXXFILES), \
    $(eval SRC_DIRS += $(patsubst %/,%,$(dir $f))))

vpath %.c $(SRC_DIRS)
vpath %.s $(SRC_DIRS)
vpath %.cc $(SRC_DIRS)
vpath %.cpp $(SRC_DIRS)

CFLAGS		+= -O0 -g -gdwarf-3
CXXFLAGS	+= -O0 -g -gdwarf-3
ASFLAGS		+= -MD

#
# Default rule is to build the executable in the executable directory
#
default: $(EXEDIR)/$(EXE)

#
# Source to Object rules
#

$(EXEDIR)/$(EXE): $(OBJS)
	@mkdir -p $(EXEDIR)
	$(CC) -o $@.debug $^ $(LIBS) $(EXTRA_LIBS)
	$(STRIP) -o $@ $@.debug

$(OBJDIR)/%.o: %.c
	@mkdir -p $(OBJDIR)
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) -c -o $@ $<

$(OBJDIR)/%.o: %.cc
	@mkdir -p $(OBJDIR)
	$(CC) $(CXXFLAGS) $(EXTRA_CXXFLAGS) -c -o $@ $<

$(OBJDIR)/%.o: %.cpp
	@mkdir -p $(OBJDIR)
	$(CC) $(CXXFLAGS) $(EXTRA_CXXFLAGS) -c -o $@ $<

$(OBJDIR)/%.o: %.s
	@mkdir -p $(OBJDIR)
	$(AS) $(ASFLAGS) $(EXTRA_ASFLAGS) -o $@ $<


#
# Utility rules
#
.PHONY: clean
clean:
	rm -rf $(OBJDIR)
	rm -f bin/*

.PHONY: vars
showvars:
	echo WIND_PROJECT_ROOT: $(WIND_PROJECT_ROOT)
	echo WIND_PROJECT_NAME: $(WIND_PROJECT_NAME)
	@echo EXEDIR: $(EXEDIR)
	@echo EXE: $(EXE)
	@echo Default target: $(EXEDIR)/$(EXE)
	@echo OBJS: $(OBJS)
	@echo SRC_DIRS $(SRC_DIRS)


