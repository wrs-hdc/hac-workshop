/*
* Copyright (c) 2016, Wind River Systems, Inc.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <wra.h>

#define DATASOURCE_NAME    "telemetry"
#define DATASOURCE_VERSION "1.0"

#define CLEANUP
#undef CLEANUP

/* main arguments 
 *  argv[1] latitude 
 *  argv[2] longitude
 *  argv[3] altitude
 *  argv[4] location tag
 *
 * for example ./location  45.351887  -75.918401  81  "Wind River"
 *
 * compile with:
 * gcc -o  location location.c -lwraclient -L/opt/hdc/lib -I/opt/hdc/include/
 */

int main(int argc, char * argv[]) {

	wra_t* agentHandle;
	wra_source_t* source;
	wra_location_t* location;
	wra_status_t status;

	if (argc != 5)
	{
		printf("usage: ./location  latitude  longitude  elevation  location-tag \n");
		printf("example usage: ./location  45.351887  -75.918401  81  \"Wind River\" \n");
		return(1);
	}

	/* initialize the HDC agent handle */
	agentHandle = wra_initialize(NULL);
	if (agentHandle ==  NULL) 
	{
		printf("wra_initialize failed\n");
		return(1);
	}
	
	/* connect the application to the HDC agent */
	status = wra_connect(agentHandle, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_connect return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* create the location and set its latitude and the longitude */ 
	location = wra_location_allocate(atof(argv[1]), atof(argv[2]));
	if (location ==  NULL) 
	{
		printf("wra_location_allocate failed\n");
		return(1);
	}
	
	/* optionally set the location altitude */
	status = wra_location_altitude_set(location, atof(argv[3]));
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_location_altitude_set return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* optionally set the location description */
	status = wra_location_tag_set(location, argv[4]);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_location_tag_set return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* send the location data to the cloud */
	status = wra_location_publish(agentHandle, location, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_location_publish return status = %s\n", wra_error(status));
		return(status);
	}
	
#ifdef CLEANUP

	/* deallocate the location */
	status = wra_metric_free(location);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_metric_free return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* disconnect the application from the HDC agent */
	status = wra_terminate(agentHandle, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_terminate return status = %s\n", wra_error(status));
		return(status);
	}
	
#endif

	return(0);
}
