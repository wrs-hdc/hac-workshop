/*
* Copyright (c) 2016, Wind River Systems, Inc.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <wra.h>
#include <mraa/gpio.h>
#include <mraa/aio.h>

#define DATASOURCE_NAME    "telemetry"
#define DATASOURCE_VERSION "1.0"

#define LOOPS 10 /* 1 */

#define CLEANUP
#undef CLEANUP

/* main arguments 
 *	argv[1] aio pin number (0 to 3)
 *	argv[2] metric name
 * 	argv[3] metric units
 *
 * for example ./data 1 temperature Celsius
 *
 * compile with:
 * gcc -o  data data.c -lwraclient -L/opt/hdc/lib -I/opt/hdc/include/
 */

float get_temperature (mraa_aio_context adc_temp, int isCelsius) {
    uint16_t adc_value = 0;
    float temp;

    adc_value = mraa_aio_read(adc_temp);

    /* calibration when using temperature sensor from grove kit */

    if (adc_value  != 0) {
	    temp = (float)(1023.0 - ((double)adc_value)) * 
			  10000.0 / ((double)adc_value);
	    temp = 1.0 / (log(temp / 10000.0) / 3975.0 + 
			  1.0 / 298.15) - 273.15;

	    if (!isCelsius) {
		temp = (temp * 1.8) + 32; 
	        fprintf(stdout, "Temperature read %d -> %.5f F°\n",
		        adc_value, temp);
	    }
	    else {
	        fprintf(stdout, "Temperature read %d -> %.5f C°\n",
		        adc_value, temp);
	    }
    } else
    {
	    fprintf(stdout, "Temperature read %d -\n",
		    adc_value);
	    temp = 0.0 /* Nan */;
    }

    return temp;
}

int main(int argc, char * argv[]) {

    wra_t*          agentHandle;
    wra_source_t*   source;
    wra_metric_t*   metric;
    wra_status_t    status;
    mraa_aio_context adc_temp;
    unsigned long aio_sensor_pin;
    float temp_value;
    unsigned int loop, i, isCelsius = 0;
    char str_unit[16];

    if (argc != 4)
	{
	printf("usage: ./data aio_pin metric unit\n");
	printf("example usage: ./data 1 temperature Celsius\n");
	printf("            or ./data 1 temperature Fahrenheit\n");

	return(1);
	}

    aio_sensor_pin  = strtol(argv[1], NULL, 10);
    if (aio_sensor_pin > 3)
    {
	printf("AIO greater than 3 : %d ...\n", aio_sensor_pin);
        return 1;
    }

    mraa_add_subplatform(MRAA_GENERIC_FIRMATA, "/dev/ttyACM0");

    /* printf("Calling mraa_aio_init(%d + 512) ...\n", aio_sensor_pin); */

    adc_temp = mraa_aio_init(aio_sensor_pin + 512);
    if (adc_temp == NULL)
    {
	printf("mraa_aio_init() FAILED ...\n");
        return 1;
    }

    /* initialize the HDC agent handle */
    agentHandle = wra_initialize(NULL);
    if (agentHandle ==  NULL) 
	{
	printf("wra_initialize failed\n");
	return(1);
	}

    /* connect the application to the HDC agent */
    status = wra_connect(agentHandle, 0);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_connect return status = %s\n", wra_error(status));
	return(status);
	}

    /* create the source, source name will be listed in the HDC console GUI */
    source = wra_source_allocate(DATASOURCE_NAME, DATASOURCE_VERSION);
    if (source ==  NULL) 
	{
	printf("wra_source_allocate failed\n");
	return(1);
	}
	
    /* create the metric, metric name will be listed in the HDC console GUI */
    metric = wra_metric_allocate(argv[2], DATASOURCE_VERSION);
    if (metric ==  NULL) 
	{
	printf("wra_metric_allocate failed\n");
	return(1);
	}

    /* set the metric type */
    status = wra_metric_type_set(metric, WRA_TYPE_FLOAT);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_metric_type_set return status = %s\n", wra_error(status));
	return(status);
	}
	
    /* set the metric units  */
    status = wra_metric_units_set(metric, argv[3]);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_metric_units_set return status = %s\n", wra_error(status));
	return(status);
	}

    strncpy (str_unit, argv[3], 8);
    i = 0;

    while (str_unit[i])
        {
	str_unit[i] = tolower(str_unit[i]);
        i++;
        }

    if (strncmp (str_unit, "celsius", 7) == 0)
	    isCelsius = 1;

    /* register the metric with the source */
    status = wra_metric_register(source, metric, 0);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_metric_register return status = %s\n", wra_error(status));
	return(status);
	}

    /* register the source with the cloud */
    status = wra_source_register(agentHandle, source, 0);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_source_register return status = %s\n", wra_error(status));
	return(status);
	}

    sleep (2);

    /* for (loop = 0; loop < 10; loop++) { */
    for (i = 0; i < LOOPS; i++) {
        /* send the metric sample to the cloud */
        temp_value = get_temperature(adc_temp, isCelsius);

        status = wra_metric_publish_float(metric, temp_value, NULL, 0);
        if (status !=  WRA_STATUS_SUCCESS) 
	    {
	    printf("wra_metric_publish_float return status = %s\n", wra_error(status));
	    /* return(status); */
	    }

	sleep (1);
    }

#ifdef CLEANUP
    /* deallocate the metric */
    status = wra_metric_free(metric, 0);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_metric_free return status = %s\n", wra_error(status));
	return(status);
	}

    /* deallocate the source */
    status = wra_source_free(source, 0);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_source_free return status = %s\n", wra_error(status));
	return(status);
	}

    /* disconnect the application from the HDC agent */
    status = wra_terminate(agentHandle, 0);
    if (status !=  WRA_STATUS_SUCCESS) 
	{
	printf("wra_terminate return status = %s\n", wra_error(status));
	return(status);
	}

    mraa_aio_close (adc_temp);
#endif

	return(0);
}
