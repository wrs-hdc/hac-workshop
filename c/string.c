/*
* Copyright (c) 2016, Wind River Systems, Inc.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <wra.h>

#define DATASOURCE_NAME    "telemetry"
#define DATASOURCE_VERSION "1.0"

#define CLEANUP
#undef CLEANUP

/* main arguments 
 *	argv[1] metric name
 *	argv[2] metric string value
 *
 * for example ./string forecast "Light snow, High 1 C, Low -1 C, P.O.P. 80%"
 *
 * compile with:
 * gcc -o  string string.c -lwraclient -L/opt/hdc/lib -I/opt/hdc/include/
 */

int main(int argc, char * argv[]) {

	wra_t* agentHandle;
	wra_source_t* source;
	wra_metric_t* metric;
	wra_status_t status;

	if (argc != 3)
	{
			printf("usage: ./string metric sample-string\n");
			printf("example usage: ./string forecast \"Light snow, High 1 C, Low -1 C, P.O.P. 80%\"\n");
			return(1);
	}

	/* initialize the HDC agent handle */
	agentHandle = wra_initialize(NULL);
	if (agentHandle ==  NULL) 
	{
		printf("wra_initialize failed\n");
		return(1);
	}

	/* connect the application to the HDC agent */
	status = wra_connect(agentHandle, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_connect return status = %s\n", wra_error(status));
		return(status);
	}

	/* create the source, source name will be listed in the HDC console GUI */
	source = wra_source_allocate(DATASOURCE_NAME, DATASOURCE_VERSION);
	if (source ==  NULL) 
	{
		printf("wra_source_allocate failed\n");
		return(1);
	}
	
	/* create the metric, metric name will be listed in the HDC console GUI */
	metric = wra_metric_allocate(argv[1], DATASOURCE_VERSION);
	if (metric ==  NULL) 
	{
		printf("wra_metric_allocate failed\n");
		return(1);
	}
	
	/* set the metric type */
	status = wra_metric_type_set(metric, WRA_TYPE_STRING);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_metric_type_set return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* register the metric with the source */
	status = wra_metric_register(source, metric, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_metric_register return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* register the source with the cloud */
	status = wra_source_register(agentHandle, source, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_source_register return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* send the metric sample to the cloud */
	status = wra_metric_publish_string(metric, argv[2], NULL, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_metric_publish_string return status = %s\n", wra_error(status));
		return(status);
	}
	
#ifdef CLEANUP

	/* deallocate the metric */
	status = wra_metric_free(metric, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_metric_free return status = %s\n", wra_error(status));
		return(status);
	}

	/* deallocate the source */
	status = wra_source_free(source, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_source_free return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* disconnect the application from the HDC agent */
	status = wra_terminate(agentHandle, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_terminate return status = %s\n", wra_error(status));
		return(status);
	}

#endif

	return(0);
}
