/*
* Copyright (c) 2016, Wind River Systems, Inc.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wra.h>

#include "mraa/gpio.h"

#define SERVICE_NAME    "actions"
#define SERVICE_VERSION "1.0"

#define D4_PIN		 (4)
#define	DEFAULT_NB_LOOPS (5)

#define CLEANUP
/* #undef CLEANUP */

static mraa_gpio_context gpio;

/* 
 * callback function, called when action on the HDC server is invoked
 */
 
wra_command_status_t commandf(wra_command_request_t *request, void* user_data)
{
	int current_state;

	/* toggle LED connected to D4 */

	current_state = mraa_gpio_read(gpio);
	if (current_state != -1)
		printf ("LED is %s (%d)\n",
			(current_state == 1) ? "ON" : "OFF", current_state);

	/* toggle LED connected to D4 */

        mraa_gpio_write(gpio, !current_state);

	printf("Callback C function executed, LED on D4 should now be %s\n",
		(current_state == 1) ? "OFF": "ON");

	return(WRA_COMMAND_COMPLETE);
}
 
 /* main arguments 
 *	argv[1] command-name
 *	argv[2] loop-number 
 *
 * for example ./action toggle_D4_LED 5
 *
 * compile with:
 * gcc -o  action action.c -lwraclient -L/opt/hdc/lib -I/opt/hdc/include/
 */
 
int main(int argc, char * argv[]) {

	wra_t* agentHandle;
	wra_service_t* service;
	wra_command_t* command;
	wra_status_t status;
	mraa_platform_t platform = mraa_get_platform_type();
	const char* board_name = mraa_get_platform_name();
        int loop, max_loops;

	if (argc != 3)
	{
			printf("usage: ./action command-name loops\n");
			printf("example usage: ./action toggle_D4_LED 5\n");
			return(1);
	}

	mraa_add_subplatform(MRAA_GENERIC_FIRMATA, "/dev/ttyACM0");

	gpio = mraa_gpio_init(D4_PIN + 512);

	if (gpio == NULL) {
	    fprintf(stdout, "Could not initilaize gpio\n");
	    return 1;
	}

	mraa_gpio_dir(gpio, MRAA_GPIO_OUT);

	/* initialize the HDC agent handle */
	agentHandle = wra_initialize(NULL);
	if (agentHandle ==  NULL) 
	{
		printf("wra_initialize failed\n");
		return(1);
	}
	
	/* connect the application to the HDC agent */
	status = wra_connect(agentHandle, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_connect return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* create the service, service name will be listed in the HDC console GUI */
	service = wra_service_allocate(SERVICE_NAME, SERVICE_VERSION );
	if (service ==  NULL) 
	{
		printf("wra_service_allocate\n");
		return(1);
	}
	
	/* create the command, command name will be listed in the HDC console GUI */
	command = wra_command_allocate(argv[1]);
	if (command ==  NULL) 
	{
		printf("wra_command_allocate\n");
		return(1);
	}

	/* register the C callback function with the command */
	status = wra_command_register_callback(command, &commandf, NULL);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_command_register_callback return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* register command with the service */
	status = wra_command_register(service, command, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_command_register return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* register service with the cloud */
	status = wra_service_register(agentHandle, service, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_service_register return status = %s\n", wra_error(status));
		return(status);
	}

	max_loops = strtol (argv[2], NULL, 10);
	if (max_loops < 1)
		max_loops = DEFAULT_NB_LOOPS;

	for (loop = 1; loop <= max_loops; loop++) {
		printf("\nbefore wra_command_wait (loop %d)\n", loop);

		/* issue a blocking call and wait for the action invocation from the cloud */
		status = wra_command_wait(agentHandle, 0);
		if (status !=  WRA_STATUS_SUCCESS) 
		{
			printf("wra_command_wait return status = %s\n", wra_error(status));
			return(status);
		}

		printf("after wra_command_wait (loop %d)\n", loop);
	}

#ifdef CLEANUP
	/* deregister the command from the service */
	status = wra_command_deregister(command, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_command_deregister return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* deallocate the command */
	status = wra_command_free(command, 0);
		if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_command_free return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* deregister the service from the cloud */
	status = wra_service_deregister(service, 0);
		if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_service_deregister return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* deallocate the service */
	status = wra_service_free(service, 0);
		if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_service_free return status = %s\n", wra_error(status));
		return(status);
	}
	
	/* disconnect the application from the HDC agent */
	status = wra_terminate(agentHandle, 0);
	if (status !=  WRA_STATUS_SUCCESS) 
	{
		printf("wra_terminate return status = %s\n", wra_error(status));
		return(status);
	}

#endif

	return(0);
}

