#include "mraa/aio.h"

int main(int argc, char * argv[])
{
    mraa_aio_context adc;
    uint16_t adc_value = 0;
    uint16_t aio_pin;
    float adc_value_float = 0.0;

    if (argc != 2)
	{
	printf("usage: ./adc_pin_read aio_pin\n");
	printf("example usage: ./adc_pin_read 1\n");

	return(1);
	}

    aio_pin  = strtol(argv[1], NULL, 10);
    if (aio_pin > 3)
	return(1);

    mraa_add_subplatform(MRAA_GENERIC_FIRMATA, "/dev/ttyACM0");

    adc = mraa_aio_init(aio_pin + 512);
    if (adc == NULL) {
        return 1;
    }

    for (;;) {
	adc_value = mraa_aio_read(adc);
	adc_value_float = mraa_aio_read_float(adc);

	fprintf(stdout, "ADC A%d read 0x%X - %d\n",
			aio_pin, adc_value, adc_value);
	fprintf(stdout, "ADC A%d read float - %.5f\n", 
			aio_pin, adc_value_float);

	sleep (1);
    }

    mraa_aio_close(adc);

    return MRAA_SUCCESS;
    }
