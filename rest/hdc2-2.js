var unirest = require('unirest');
var mod = require('./mkrequest')
var mk = mod.mkrequest;
var im = mod.svc.im;

function testRest() {
    unirest.get(mk(im + '/issues','?'))
    .auth(mod.user, mod.pass)
    .headers(

        { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json'
        }
    )

    .send()

    .end(function(response) {

        for (i=response.body.data.startIndex;
             i<response.body.data.startIndex +
               response.body.data.currentItemCount; i++) {
            console.log(
                response.body.data.items[i].creator + '    ' +
                response.body.data.items[i].creationDate + '    ' +
                response.body.data.items[i].name
                )
        }

    })

};

testRest()
