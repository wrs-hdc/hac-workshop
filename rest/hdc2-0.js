var unirest = require('unirest');
var mod = require('./mkrequest')
var mk = mod.mkrequest;
var sm = mod.svc.sm;

function testRest() {
    unirest.get(mk(sm + '/devices', '?'))
    .auth(mod.user, mod.pass)
    .headers(

        { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json'
        }
    )

    .send()

    .end(function (response) {
        
        for (i=response.body.data.startIndex;
      		 i<response.body.data.startIndex +
      		   response.body.data.currentItemCount; i++) {
        	console.log(
        	    response.body.data.items[i].id + '    ' +
        	    response.body.data.items[i].uuid + '	' +
        	    response.body.data.items[i].properties.hostname
        	    )
    	}

    })
};

testRest()
