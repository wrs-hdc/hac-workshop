var unirest = require('unirest');
var mod = require('./mkrequest')
var mk = mod.mkrequest;
var svc = mod.svc;

function testRest() {
    unirest.get(mk(svc.um + '/users','?'))
    .auth(mod.user, mod.pass)
    .headers(

        { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json'
        }
    )

    .send()

    .end(function(response) {
        console.log(response.body)
    })

};

testRest()
