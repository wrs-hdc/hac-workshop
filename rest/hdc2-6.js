var unirest = require('unirest');
var mod = require('./mkrequest')
var mk = mod.mkrequest;
var svc = mod.svc;

var uuid = '74f22231-487b-cb1c-16cc-a439fc5b55e6';
var query = '{deviceId:{"$in":[\"' + uuid + '\"]}}';

function testRest() {
    unirest.get(mk(svc.tm + '/metricrecordsnapshots?$filter=' + query, '&'))
    .auth(mod.user, mod.pass)
    .headers(

        { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json'
        }
    )

    .send()

    .end(function(response) {
        for (i=0; i<response.body.data.currentItemCount; i++) {
        	console.log(
        	    response.body.data.items[i].timestamp,
        	    response.body.data.items[i].metricName,
        	    response.body.data.items[i].value
        	    )
    	}
    })

};

testRest()
