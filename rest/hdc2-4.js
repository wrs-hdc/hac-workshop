var unirest = require('unirest');
var mod = require('./mkrequest')
var mk = mod.mkrequest;
var rm = mod.svc.rm;

function testRest() {
    unirest.get(mk(rm + '/rules','?'))
    .auth(mod.user, mod.pass)
    .headers(

        { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json'
        }
    )

    .send()

    .end(function(response) {

        for (i=response.body.data.startIndex;
             i<response.body.data.startIndex +
               response.body.data.currentItemCount; i++) {
            console.log(
                response.body.data.items[i].name + '    ' +
                response.body.data.items[i].createdOn + '    ' +
                response.body.data.items[i].eventType
                )
        }

    })

};

testRest()
