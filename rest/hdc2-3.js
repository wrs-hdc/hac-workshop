var unirest = require('unirest');
var mod = require('./mkrequest')
var mk = mod.mkrequest;
var sm = mod.svc.sm;

var uuid = '74f22231-487b-cb1c-16cc-a439fc5b55e6';

function testRest() {
    unirest.get(mk(sm + '/devices/' + uuid,'?'))
    .auth(mod.user, mod.pass)
    .headers(

        { 
            'Content-Type': 'application/json',
            'Accept' : 'application/json'
        }
    )

    .send()

    .end(function(response) {

        console.log(
            response.body['device-status'].lastUpdateStatus + '    ' +
            response.body['iot-properties'].device_address + '    ' +
            response.body.properties.hostname
        )

    })

};

testRest()
